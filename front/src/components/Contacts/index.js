import React from 'react';
import Person from './Person';
import './styles.css';

const Contacts = ({ persons = [] }) => {
  return (
    <div className='sidebar__contacts'>
      {persons.map(person => (
        <Person
          key={person.uuid}
          fullName={person.fullName}
          lastActivity={person.lastActivity}
          profilePictureURL={person.profilePictureURL}
        />
      ))}
    </div>
  );
};

export default Contacts;
