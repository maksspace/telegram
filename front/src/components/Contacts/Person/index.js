import React from 'react';
import './styles.css';

const Person = ({ fullName, lastActivity, profilePictureURL }) => {
  return (
    <div className='contacts__person'>
      <div className='contacts__person__picture'>
        {profilePictureURL ? (
          <img src={profilePictureURL} />
        ) : (
          fullName.slice(0, 2)
        )}
      </div>
      <div className='contacts__person__details'>
        <div>
          <div className='contacts__person__details__name'>{fullName}</div>
          <div className='contacts__person__details__last-activity'>
            {lastActivity}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Person;
