import React from 'react';
import Navigation from './Navigation';
import Contacts from '../Contacts';
import './styles.css';

const persons = [
  {
    uuid: '11',
    fullName: 'Maks',
    lastActivity: 'today',
    profilePictureURL: ''
  },
  {
    uuid: '12',
    fullName: 'Mak1',
    lastActivity: 'today',
    profilePictureURL: ''
  },
  {
    uuid: '13',
    fullName: 'Mak2',
    lastActivity: 'today',
    profilePictureURL: ''
  },
  {
    uuid: '14',
    fullName: 'Maks3',
    lastActivity: 'today',
    profilePictureURL: ''
  },
  {
    uuid: '15',
    fullName: 'Mak4',
    lastActivity: 'today',
    profilePictureURL: ''
  },
  {
    uuid: '16',
    fullName: 'Mak5',
    lastActivity: 'today',
    profilePictureURL: ''
  },
  {
    uuid: '11',
    fullName: 'Maks',
    lastActivity: 'today',
    profilePictureURL: ''
  },
  {
    uuid: '12',
    fullName: 'Mak1',
    lastActivity: 'today',
    profilePictureURL: ''
  },
  {
    uuid: '13',
    fullName: 'Mak2',
    lastActivity: 'today',
    profilePictureURL: ''
  },
  {
    uuid: '14',
    fullName: 'Maks3',
    lastActivity: 'today',
    profilePictureURL: ''
  },
  {
    uuid: '15',
    fullName: 'Mak4',
    lastActivity: 'today',
    profilePictureURL: ''
  },
  {
    uuid: '16',
    fullName: 'Mak5',
    lastActivity: 'today',
    profilePictureURL: ''
  },
  {
    uuid: '11',
    fullName: 'Maks',
    lastActivity: 'today',
    profilePictureURL: ''
  },
  {
    uuid: '12',
    fullName: 'Mak1',
    lastActivity: 'today',
    profilePictureURL: ''
  },
  {
    uuid: '13',
    fullName: 'Mak2',
    lastActivity: 'today',
    profilePictureURL: ''
  },
  {
    uuid: '14',
    fullName: 'Maks3',
    lastActivity: 'today',
    profilePictureURL: ''
  },
  {
    uuid: '15',
    fullName: 'Mak4',
    lastActivity: 'today',
    profilePictureURL: ''
  },
  {
    uuid: '16',
    fullName: 'Mak5',
    lastActivity: 'today',
    profilePictureURL: ''
  },
  {
    uuid: '11',
    fullName: 'Maks',
    lastActivity: 'today',
    profilePictureURL: ''
  },
  {
    uuid: '12',
    fullName: 'Mak1',
    lastActivity: 'today',
    profilePictureURL: ''
  },
  {
    uuid: '13',
    fullName: 'Mak2',
    lastActivity: 'today',
    profilePictureURL: ''
  },
  {
    uuid: '14',
    fullName: 'Maks3',
    lastActivity: 'today',
    profilePictureURL: ''
  },
  {
    uuid: '15',
    fullName: 'Mak4',
    lastActivity: 'today',
    profilePictureURL: ''
  },
  {
    uuid: '16',
    fullName: 'Mak5',
    lastActivity: 'today',
    profilePictureURL: ''
  },
  {
    uuid: '11',
    fullName: 'Maks',
    lastActivity: 'today',
    profilePictureURL: ''
  },
  {
    uuid: '12',
    fullName: 'Mak1',
    lastActivity: 'today',
    profilePictureURL: ''
  },
  {
    uuid: '13',
    fullName: 'Mak2',
    lastActivity: 'today',
    profilePictureURL: ''
  },
  {
    uuid: '14',
    fullName: 'Maks3',
    lastActivity: 'today',
    profilePictureURL: ''
  },
  {
    uuid: '15',
    fullName: 'Mak4',
    lastActivity: 'today',
    profilePictureURL: ''
  },
  { uuid: '16', fullName: 'Mak5', lastActivity: 'today', profilePictureURL: '' }
];

const Sidebar = () => {
  return (
    <div className='app__sidebar'>
      <Contacts persons={persons} />
      <Navigation />
    </div>
  );
};

export default Sidebar;
