import React from 'react';
import Icon from '../../../ui-library/Icon';
import './styles.css';

const Navigation = () => {
  return (
    <div className='sidebar__navigation'>
      <Icon icon='supervised_user_circle' />
      <Icon icon='local_phone' />
      <Icon icon='comment' />
      <Icon icon='settings' />
    </div>
  );
};

export default Navigation;
