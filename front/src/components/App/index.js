import React from 'react';
import Sidebar from '../Sidebar';
import Content from '../Content';

const App = () => {
  return (
    <>
      <Sidebar />
      <Content />
    </>
  );
};

export default App;
