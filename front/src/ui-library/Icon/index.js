import React from 'react';
import './styles.css';
const Icon = ({ icon }) => {
  return <i className='material-icons ui-icon'>{icon}</i>;
};

export default Icon;
